package dev.tacon.interfaces.reference;

import dev.tacon.interfaces.dto.DtoWithIntId;

/**
 * Represents a reference to a data transfer object (DTO) with an associated integer.
 *
 * @param <D> the type of DTO.
 */
public class DtoIntRef<D extends DtoWithIntId<D>> extends DtoRef<D, Integer> {

	private static final long serialVersionUID = -7191658974183485229L;

	/**
	 * Protected required default constructor.
	 * Only for internal usage, not public!
	 */
	protected DtoIntRef() {
		super();
	}

	/**
	 * Constructor that takes a primitive {@code int} ID for the DTO.
	 *
	 * @param id the identifier of the DTO.
	 */
	public DtoIntRef(final int id) {
		super(Integer.valueOf(id));
	}

	/**
	 * Constructor that takes an {@code Integer} ID for the DTO.
	 *
	 * @param id the identifier of the DTO. Must not be {@code null}.
	 * @throws NullPointerException if the provided ID is {@code null}.
	 */
	public DtoIntRef(final Integer id) {
		super(id);
	}
}

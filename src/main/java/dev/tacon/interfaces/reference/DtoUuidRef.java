package dev.tacon.interfaces.reference;

import java.util.UUID;

import dev.tacon.interfaces.dto.DtoWithUuid;

/**
 * Represents a reference to a data transfer object (DTO) with an associated string.
 *
 * @param <D> the type of DTO.
 */
public class DtoUuidRef<D extends DtoWithUuid<D>> extends DtoRef<D, UUID> {

	private static final long serialVersionUID = 926635774339007814L;

	/**
	 * Protected required default constructor.
	 * Only for internal usage, not public!
	 */
	protected DtoUuidRef() {
		super();
	}

	/**
	 * Constructor that takes a {@code UUID} ID for the DTO.
	 *
	 * @param id the identifier of the DTO. Must not be {@code null}.
	 * @throws NullPointerException if the provided ID is {@code null}.
	 */
	public DtoUuidRef(final UUID id) {
		super(id);
	}
}

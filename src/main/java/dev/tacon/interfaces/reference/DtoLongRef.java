package dev.tacon.interfaces.reference;

import dev.tacon.interfaces.dto.DtoWithLongId;

/**
 * Represents a reference to a data transfer object (DTO) with an associated long.
 *
 * @param <D> the type of DTO.
 */
public class DtoLongRef<D extends DtoWithLongId<D>> extends DtoRef<D, Long> {

	private static final long serialVersionUID = -3652131357431256774L;

	/**
	 * Protected required default constructor.
	 * Only for internal usage, not public!
	 */
	protected DtoLongRef() {
		super();
	}

	/**
	 * Constructor that takes a primitive {@code long} ID for the DTO.
	 *
	 * @param id the identifier of the DTO.
	 */
	public DtoLongRef(final Long id) {
		super(id);
	}

	/**
	 * Constructor that takes an {@code Long} ID for the DTO.
	 *
	 * @param id the identifier of the DTO. Must not be {@code null}.
	 * @throws NullPointerException if the provided ID is {@code null}.
	 */
	public DtoLongRef(final long id) {
		super(Long.valueOf(id));
	}
}

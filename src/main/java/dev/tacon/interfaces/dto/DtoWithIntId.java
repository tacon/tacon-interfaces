package dev.tacon.interfaces.dto;

import dev.tacon.annotations.NonNull;
import dev.tacon.interfaces.reference.DtoIntRef;

/**
 * Represents a data transfer object (DTO) with an associated integer ID and reference.
 *
 * @param <D> the type of DTO.
 */
public class DtoWithIntId<D extends DtoWithIntId<D>> extends DtoWithId<D, Integer, DtoIntRef<D>> {

	private static final long serialVersionUID = 7459469513673286979L;

	/**
	 * Protected required default constructor.
	 * Initializes with a {@code null} reference.
	 */
	protected DtoWithIntId() {
		super();
	}

	/**
	 * Constructor to initialize with a primitive int ID.
	 *
	 * @param id the integer ID to set.
	 */
	protected DtoWithIntId(final int id) {
		this(Integer.valueOf(id));
	}

	/**
	 * Constructor to initialize with an Integer ID.
	 *
	 * @param id the Integer ID to set, not null.
	 */
	protected DtoWithIntId(final @NonNull Integer id) {
		super(new DtoIntRef<>(id));
	}

	/**
	 * Constructor to initialize with a specific reference.
	 *
	 * @param ref the reference to set, or {@code null}.
	 */
	protected DtoWithIntId(final DtoIntRef<D> ref) {
		super(ref);
	}
}

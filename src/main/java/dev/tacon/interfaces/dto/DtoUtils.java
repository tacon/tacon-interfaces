package dev.tacon.interfaces.dto;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import dev.tacon.annotations.Nullable;
import dev.tacon.interfaces.reference.DtoIntRef;
import dev.tacon.interfaces.reference.DtoLongRef;
import dev.tacon.interfaces.reference.DtoRef;
import dev.tacon.interfaces.reference.DtoStringRef;
import dev.tacon.interfaces.reference.DtoUuidRef;

/**
 * Utility class for working with DTOs (Data Transfer Objects) and DTO references.
 * This class provides methods for converting between DTOs and their references,
 * as well as obtaining IDs from these objects.
 */
public final class DtoUtils {

	/**
	 * Retrieves the corrisponding set of references from a collection of DTOs.
	 *
	 * @param dtos the collection of DTOs.
	 * @return the set of references, or {@code null} if the input is {@code null}.
	 */
	public static <D extends DtoWithId<D, K, R>, K, R extends DtoRef<D, K>> Set<R> refs(final @Nullable Collection<D> dtos) {
		return dtos != null ? dtos.stream().map(DtoUtils::ref).collect(Collectors.toSet()) : null;
	}

	/**
	 * Retrieves the reference from a single DTO.
	 *
	 * @param dto the DTO to get the reference from.
	 * @return the reference, or {@code null} if the DTO is {@code null}.
	 */
	public static <D extends DtoWithId<D, K, R>, K, R extends DtoRef<D, K>> R ref(final @Nullable D dto) {
		return dto != null ? dto.getRef() : null;
	}

	/**
	 * Retrieves the corrisponding set of IDs from a collection of DTO references.
	 *
	 * @param refs the collection of DTO references.
	 * @return the set of IDs, or {@code null} if the input is {@code null}.
	 */
	public static <K> Set<K> refIds(final @Nullable Collection<? extends DtoRef<?, K>> refs) {
		return refs != null ? refs.stream().map(DtoUtils::refId).collect(Collectors.toSet()) : null;
	}

	/**
	 * Retrieves the corrisponding set of IDs from a collection of DTOs.
	 *
	 * @param dtos the collection of DTOs.
	 * @return the set of IDs, or {@code null} if the input is {@code null}.
	 */
	public static <D extends DtoWithId<D, K, ?>, K> Set<K> ids(final @Nullable Collection<D> dtos) {
		return dtos != null ? dtos.stream().map(DtoUtils::id).collect(Collectors.toSet()) : null;
	}

	/**
	 * Retrieves the ID from a single DTO reference.
	 *
	 * @param ref the DTO reference to get the ID from.
	 * @return the ID, or {@code null} if the reference is {@code null}.
	 */
	public static <K> K refId(final @Nullable DtoRef<?, K> ref) {
		return ref != null ? ref.getId() : null;
	}

	/**
	 * Retrieves the ID from a single DTO.
	 *
	 * @param dto the DTO to get the ID from.
	 * @return the ID, or {@code null} if the DTO is {@code null}.
	 */
	public static <D extends DtoWithId<D, K, ?>, K> K id(final @Nullable D dto) {
		return dto != null ? dto.getId() : null;
	}

	/**
	 * Creates a reference for a DTO object with a UUID key.
	 *
	 * @param key the UUID key.
	 * @return a DTO UUID reference, or {@code null} if the key is {@code null}.
	 */
	public static <D extends DtoWithUuid<D>> DtoUuidRef<D> ref(final @Nullable UUID key) {
		return key != null ? new DtoUuidRef<>(key) : null;
	}

	/**
	 * Creates a reference for a DTO object with a String key.
	 *
	 * @param key the String key.
	 * @return a DTO String reference, or {@code null} if the key is {@code null}.
	 */
	public static <D extends DtoWithStringId<D>> DtoStringRef<D> ref(final @Nullable String key) {
		return key != null ? new DtoStringRef<>(key) : null;
	}

	/**
	 * Creates a reference for a DTO object with an Integer key.
	 *
	 * @param key the Integer key.
	 * @return a DTO Integer reference, or {@code null} if the key is {@code null}.
	 */
	public static <D extends DtoWithIntId<D>> DtoIntRef<D> ref(final @Nullable Integer key) {
		return key != null ? new DtoIntRef<>(key) : null;
	}

	/**
	 * Creates a reference for a DTO object with a Long key.
	 *
	 * @param key the Long key.
	 * @return a DTO Long reference, or {@code null} if the key is {@code null}.
	 */
	public static <D extends DtoWithLongId<D>> DtoLongRef<D> ref(final @Nullable Long key) {
		return key != null ? new DtoLongRef<>(key) : null;
	}

	private DtoUtils() {
		throw new AssertionError();
	}
}
package dev.tacon.interfaces.dto;

import java.util.Objects;

import dev.tacon.interfaces.reference.DtoRef;

/**
 * Represents a data transfer object (DTO) with an associated ID and reference.
 *
 * @param <D> the type of DTO.
 * @param <I> the type of ID for the DTO.
 * @param <R> the type of reference for the DTO.
 */
public class DtoWithId<D extends Dto, I, R extends DtoRef<D, I>> implements Dto {

	private static final long serialVersionUID = -3716820606644291269L;

	private R ref;

	/**
	 * Protected required default constructor.
	 * Initializes with a {@code null} reference.
	 */
	protected DtoWithId() {}

	/**
	 * Constructor to initialize with a reference.
	 *
	 * @param ref the reference to set.
	 */
	protected DtoWithId(final R ref) {
		this.ref = ref;
	}

	/**
	 * Retrieves the reference of the DTO.
	 *
	 * @return the reference, or {@code null} if it's not set.
	 */
	public R getRef() {
		return this.ref;
	}

	/**
	 * Retrieves the ID from the DTO's reference.
	 *
	 * @return the ID, or {@code null} if the reference is {@code null}.
	 */
	public I getId() {
		return this.ref == null ? null : this.ref.getId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		if (this.ref == null || ((DtoWithId<?, ?, ?>) obj).ref == null) {
			return false;
		}
		return Objects.equals(this.ref, ((DtoWithId<?, ?, ?>) obj).ref);
	}

	@Override
	public int hashCode() {
		return this.ref == null ? 0 : this.ref.hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ",ref=" + this.ref;
	}
}

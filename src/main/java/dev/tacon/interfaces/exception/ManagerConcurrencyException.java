package dev.tacon.interfaces.exception;

import java.io.Serializable;

/**
 * Exception representing a concurrency issue in a Jakarta EE environment.
 * Typically thrown when there is a mismatch between the expected and current version of a managed object.
 */
public class ManagerConcurrencyException extends RuntimeException {

	private static final long serialVersionUID = -722273837586030163L;
	private final Serializable currentVersion;
	private final Serializable expectedVersion;
	private final String className;
	private final String entityId;

	/**
	 * Constructs a new {@code ManagerConcurrencyException} with current version and expected version.
	 *
	 * @param currentVersion the current version of the object that caused the exception.
	 * @param expectedVersion the expected version of the object.
	 */
	public ManagerConcurrencyException(final String className, final String entityId, final Serializable currentVersion, final Serializable expectedVersion) {
		super("Expected version " + expectedVersion + " but current version is " + currentVersion);
		this.className = className;
		this.entityId = entityId;
		this.currentVersion = currentVersion;
		this.expectedVersion = expectedVersion;
	}

	public String getClassName() {
		return this.className;
	}

	public String getEntityId() {
		return this.entityId;
	}

	/**
	 * Retrieves the current version of the object that caused the exception.
	 *
	 * @return the current version. May be {@code null} if the version is not applicable.
	 */
	public Serializable getCurrentVersion() {
		return this.currentVersion;
	}

	/**
	 * Retrieves the expected version of the object that was supposed to be managed.
	 *
	 * @return the expected version. May be {@code null} if the version is not applicable.
	 */
	public Serializable getExpectedVersion() {
		return this.expectedVersion;
	}
}

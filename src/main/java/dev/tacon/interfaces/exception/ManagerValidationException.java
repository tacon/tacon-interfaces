package dev.tacon.interfaces.exception;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

/**
 * Custom exception class that represents validation issues on DTOs represented
 * by a list of {@link ValidationError}s for more detailed error reporting.
 */
public abstract class ManagerValidationException extends ManagerException {

	private static final long serialVersionUID = -593270702974639168L;

	public static final String ERROR_CODE = "dev.tacon.validation";

	private final List<ValidationError> validationErrors;

	/**
	 * Constructs a new exception with the specified list of validation errors.
	 *
	 * @param validationErrors the list of validation errors, not null.
	 */
	public ManagerValidationException(final List<ValidationError> validationErrors) {
		super(ERROR_CODE);
		this.validationErrors = requireNonNull(validationErrors, "errors cannot be null");
	}

	/**
	 * Gets the list of validation errors associated with this exception.
	 *
	 * @return the list of validation errors.
	 */
	public List<ValidationError> getValidationErrors() {
		return this.validationErrors;
	}

	/**
	 * Detailed information about a single validation error.
	 *
	 * @param annotation the annotation that triggered the validation failure.
	 * @param rootBean the root bean instance where the validation failure occurred.
	 * @param invalidValue the value that failed validation.
	 * @param field indicate if the validation error is associated with a field.
	 * @param propertyPath the property path leading to the failure.
	 * @param message the default human-readable message describing the validation error.
	 * @param messageKey the key used to fetch an i18n message for this validation error.
	 * @param messageParams additional parameters that can be used for more detailed error messages.
	 *
	 */
	public record ValidationError(
			Annotation annotation,
			Serializable rootBean,
			Serializable invalidValue,
			boolean field,
			String fullFieldName,
			String propertyPath,
			String message,
			String messageKey,
			Map<String, Serializable> messageParams) implements Serializable {

		private static final long serialVersionUID = 6556720269128436119L;
	}
}

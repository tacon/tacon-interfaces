package dev.tacon.interfaces.ext;

/**
 * Provides an interface to be implemented by objects that have an associated "insert ID".
 * This is particularly useful for DTOs (Data Transfer Objects) that are used in a Jakarta EE environment
 * where the object's ID is set upon insertion into a data store.
 *
 * @param <T> the type of the insert ID and ID of the object. This is generally expected to match the DTO's key class type.
 */
public interface HasInsertId<T> {

	/**
	 * Retrieves the insert ID associated with this object.
	 *
	 * @return the insert ID. May return {@code null} if the ID has not been set.
	 */
	T getInsertId();

	/**
	 * Sets the insert ID for this object.
	 *
	 * @param insertId the insert ID to be set. Can be {@code null}.
	 */
	void setInsertId(T insertId);

	/**
	 * Retrieves the ID associated with this object. This method forces the generic type
	 * to be the same as the object's key class.
	 *
	 * @return the object's ID. May return {@code null} if the ID has not been set.
	 */
	T getId();
}

package dev.tacon.interfaces.ext;

import java.io.Serializable;

/**
 * Provides an interface to be implemented by objects that have an associated version.
 * The versioning mechanism can be useful for handling optimistic concurrency in a Jakarta EE environment.
 * The version type must implement {@link Serializable} and {@link Comparable}.
 *
 * @param <V> the type of the version, which must be comparable.
 */
public interface HasVersion<V extends Serializable & Comparable<? super V>> {

	/**
	 * Retrieves the version associated with this object.
	 *
	 * @return the object's version. May return {@code null} if the version has not been set.
	 */
	V getVersion();

	/**
	 * Sets the version for this object.
	 *
	 * @param version the version to be set. Can be {@code null}.
	 */
	void setVersion(V version);
}

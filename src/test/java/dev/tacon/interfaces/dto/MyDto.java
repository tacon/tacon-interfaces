package dev.tacon.interfaces.dto;

import dev.tacon.interfaces.reference.DtoIntRef;

public class MyDto extends DtoWithIntId<MyDto> {

	private static final long serialVersionUID = 123456789L;
	private String name;

	public MyDto() {
		super();
	}

	public MyDto(final int id) {
		super(id);
	}

	public MyDto(final Integer id) {
		super(id);
	}

	public MyDto(final DtoIntRef<MyDto> ref) {
		super(ref);
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}
}